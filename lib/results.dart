import 'dart:convert';
import 'package:bigdatacorp_apps_liveness_v3_sdk/bigdatacorp_apps_liveness_v3_sdk.dart';
import 'package:flutter/material.dart';

class ResultsPage extends StatelessWidget {
  final RetrievalResponse receivedData; // Assuming received data is a map

  const ResultsPage({Key? key, required this.receivedData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Container(
        color: Colors.grey[800],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/white_logo.png', fit: BoxFit.cover),
            const SizedBox(height: 50), // Add some spacing
            Text('TicketId: ${receivedData.ticketId}', selectionColor: Colors.white),
            const SizedBox(height: 25), // Add some spacing
            Text('TicketId: ${receivedData.resultMessage}', selectionColor: Colors.white),
            const SizedBox(height: 25), // Add some spacing
            Text('Score: ${receivedData.score}', selectionColor: Colors.white,),
            const SizedBox(height: 25), // Add some spacing

            Image.memory(
              base64Decode(receivedData.auditImageBytes!),
              width: MediaQuery.of(context).size.width * 0.8, // Adjust width as needed
              fit: BoxFit.cover, // Adjust fit as needed
            ),
            const SizedBox(height: 50), // Add some spacing

            ElevatedButton(
              onPressed: () => Navigator.of(context).popUntil((route) => route.isFirst),
              child: const Text('Voltar'),
            ),
          ],
        ),
      ),
    ));
  }
}
