import 'package:bigdatacorp_apps_liveness_v3_sdk/bigdatacorp_apps_liveness_v3_sdk.dart';
import 'package:flutter/material.dart';
import 'package:flutter_liveness_v3_demoapp/results.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a purple toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController titleController = TextEditingController();
  TextEditingController bodyController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.grey[800],
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Image.asset('assets/images/white_logo.png', fit: BoxFit.cover),
            const SizedBox(height: 100), // Add some spacing

            Image.asset('assets/images/liveness_icon.png'),
            const SizedBox(height: 100), // Add some spacing

            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BigIdLivenessV3(
                          token:
                          'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6InRoYWRldS5iYXJyb3NAYmlnZGF0YWNvcnAuY29tLmJyIiwibmJmIjoxNzE2NDE5NTcyLCJleHAiOjE3MTY1MDU5NzIsImlhdCI6MTcxNjQxOTU3MiwiaXNzIjoiQmlnIERhdGEgQ29ycC4iLCJwcm9kdWN0cyI6W10sImRvbWFpbiI6IkFETUlOIn0.O6WBtWzniD831SFQO9mosJpT0eO-gzm3oeCvH3sLCWM',
                          onFinish: (retrieveResp) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ResultsPage(
                                      receivedData:
                                      retrieveResp as RetrievalResponse)),
                            );
                          })),
                );
              },
              child: const Text("Iniciar prova de Vida"),
            ),
          ]),
        ),
      ),
    );
  }

  void display(String s) {
    print(s);
  }
}
